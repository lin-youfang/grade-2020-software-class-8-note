<?php
//1. 表单填写后，提交到服务端。
//	2. 账号长度4 ~ 16，如果不符合，给出提示信息。
//	3. 密码长度6 ~ 15，如果不符合，给出提示信息。
//	4. 性别必须选择，如果不符合，给出提示信息。
//	5. 爱好至少填写一项，如果不符合，给出提示信息。
//	6. 籍贯必须选择，如果没有选择，给出提示信息。
//	7. 自我介绍长度10 ~ 200，如果不符合，给出提示信息。
//	8. 头像需要上传，大小200k以内，如果不符合，给出提示信息。
var_dump($_POST);
$account=$_POST['account'] ?? '';
echo mb_strlen($account);
if(!mb_strlen($account)>4 && !mb_strlen($account)<16){
    echo "账号长度为：4-16";
}
$password=$_POST['password'] ?? '';
if(!mb_strlen($password)>6 && !mb_strlen($password)<15){
    echo "密码长度为：6-15";

}
$sex=$_POST['sex'] ?? '性别必须选择一项';
$city=$_POST['city'] ?? '籍贯不能为空';
$hobby=$_POST['hobby'] ?? [];
?>
<?php
$sexList=[1 =>'男', 2 =>'女'];
$hobbyList=[1 =>'篮球',2 =>'足球',3 =>'排球'];
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<form action="demo02.php" method="post">
    <p>账号：<input type="password" name="account"/></p>
    <p>密码：<input type="password" name="password"/></p>
    <p>性别：<input type="radio" name="sex" value="1"/>男
        <input type="radio" name="sex" value="2"/>女

    </p>

    <p>籍贯：
        <select name="city">
            <option>泉州</option>
            <option>厦门</option>
            <option>龙岩</option>
        </select>
    </p>
    <p>爱好：
        <input type="checkbox" name="hobby[]" value="1"/>篮球
        <input type="checkbox" name="hobby[]" value="2"/>足球
        <input type="checkbox" name="hobby[]" value="3"/>排球
    </p>
    <p>自我介绍：
        <textarea name="introduction"></textarea>
    </p>
    <input type="submit" value="提交"/>

</form>

</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<p>你输入的账号：<?php echo $account ?></p>
<p>你输入的密码：<?php echo $password ?></p>
<p>你输入的性别：<?php echo $sexList[$sex]?? ''; ?></p>
<p>你的籍贯：<?php echo $city ?></p>
<p>你的爱好：
<?php
foreach ($hobby as $key => $value){

    echo $hobbyList[$value] .',';
}
?>
</p>

</body>
</html>