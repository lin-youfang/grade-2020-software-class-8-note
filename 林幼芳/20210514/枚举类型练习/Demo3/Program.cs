﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo3
{
    class Program
    {
        public enum game
        {
            战士,
            法师,
            精灵

        }
        public static void playgame(game game) {
            switch (game)
            {
                case game.战士:
                    Console.WriteLine(game+"技能为：碎石打击、烈焰锚钩、战斗咆哮");
                    break;
                case game.法师:
                    Console.WriteLine(game+"技能为：巨浪冲击、元素突击、复仇杀戮");
                    break;
                case game.精灵:
                    Console.WriteLine(game+"技能为：减速陷阱、能量浪潮、旋风剑舞");
                    break;
                default:
                    Console.WriteLine(game+"没有技能");
                    break;
            }

        }
        static void Main(string[] args)
        {
            //2、RPG游戏中，通常有不同的职业，比如“战士”、“法师”、“精灵”等等职业，
            //A、现在请定义一个游戏职业的枚举。
            //B、然后定一个输出职业技能的方法，根据传入的职业枚举的值来输出，
            //战士的技能：碎石打击、烈焰锚钩、战斗咆哮
            //法师的技能：巨浪冲击、元素突击、复仇杀戮
            //精灵的技能：减速陷阱、能量浪潮、旋风剑舞

            Console.WriteLine("请输入要选择的职业");
            int n = int.Parse(Console.ReadLine());
            game game=(game)n;
            playgame(game);




        }
    }
}
