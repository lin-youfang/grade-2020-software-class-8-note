$(function() {
    // 点击全选按钮，所有商品的复选框状态会变更
    $('.checkall').click(function() {
        $('.j-checkbox').prop('checked', $(this).prop('checked'));
        $('.checkall').prop('checked', $(this).prop('checked'));
    });

    // 商品复选框选中状态切换，全选状态也会切换
    $('.j-checkbox').click(function() {
        // 判断选中的数量和所有复选框的数量是否一致
        let len1 = $('.j-checkbox').length;
        let len2 = $('.j-checkbox:checked').length;
        if (len1 === len2) {
            $('.checkall').prop('checked', true);
        } else {
            $('.checkall').prop('checked', false);
        }
        getSum()
    });

    // 点击+号，增加数量
    $('.increment').click(function() {
        let num = $(this).siblings('.itxt').val();
        num++;
        $(this).siblings('.itxt').val(num);

        // 修改小计的值
        let price = $(this).closest('.p-num').siblings('.p-price').text();
        let price2 = price.substr(1, price.length - 1);
        let sum = num * Number(price2);
        $(this).closest('.p-num').siblings('.p-sum').text('￥' + sum.toFixed(2));
        getSum()
    });

    $('.decrement').click(function() {
        let num = $(this).siblings('.itxt').val();
        if (num > 1) {
            num--;
            $(this).siblings('.itxt').val(num);

            let price = $(this).closest('.p-num').siblings('.p-price').text();
            let price2 = price.substr(1, price.length - 1);
            let sum = num * Number(price2);
            $(this).closest('.p-num').siblings('.p-sum').text('￥' + sum.toFixed(2));
        }
        getSum()
    });
    //计算总计和总额模块
    function getSum(){
        let count=0;//总件数
        let money=0;//总额
        //1.遍历文本框的值
        $('.itxt').each(function(i,ele){
            count+=parseInt($(ele).val());
        })
        //修改总件数文本值
        $(".amount-sum em").text(count);
        //计算总额
        $('.p-sum').each(function(i,ele){
            //去掉￥符号
            money+=Number($(ele).text().substr(1));
        })
        //修改总金额文本值
        //只取两位小数
        $(".price-sum em").text('￥'+money.toFixed(2));
    }
})