use master;
go
-- 检测原库是否存在，如果存在则删除
if exists (select * from sys.databases where name = 'Student')
	drop database Student;
go
-- 创建一个库
create database Student;
go
-- 使用这个库
use Student;
go


--------------------
-- 建表部分
--------------------
-- 创建班级表，存储班级信息，其中字段包含：班级id、班级名称
create table Class(
	ClassId int not null identity(1,1),
	ClassName nvarchar(50) not null
);
go

-- 创建学生表，存储学生信息，其中字段保护：学生id、姓名、性别、生日、家庭住址，所属班级id
create table Student (
	StudentId int not null identity(1, 1),
	StudentName nvarchar(50),
	StudentSex tinyint not null,
	StudentBirth date,
	StudentAddress nvarchar(255) not null
);
go

-- 创建课程表，存储课程信息，其中字段包含：课程id、课程名称、课程学分
create table Course(
	CourseId int identity(1,1),
	CourseName nvarchar(50),
	CourseCredit int
);
go

-- 创建班级课程表，存储班级课程信息，其中字段包含：自增id、班级id、课程id
create table ClassCourse(
	ClassCourseId int identity(1,1),
	ClassId int,
	CourseId int
);
go

-- 创建分数表，存储学生每个课程分数信息，其中字段包含：分数id、学生id、课程id、分数
create table Score(
	ScoreId int identity(1,1),
	StudentId int,
	CourseId int,
	Score int
);
go


-- 学生表建好了，细想一下少了一个所属班级的字段
-- 给学生表 Student 增加一个所属班级id字段 
alter table Student add ClassId int not null;
go

----------------------------------------
-- 创建约束部分，使用alter进行修改
----------------------------------------
-- 班级表 ClassId 字段需要设置为主键（主键约束）
alter table Class add constraint PK_Class_ClassId primary key (ClassId);
-- 学生表 StudentId 字段需要设置为主键（主键约束）
alter table Student add constraint PK_Student_StudentId primary key (StudentId);
-- 课程表 CourseId 字段需要设置为主键（主键约束）
alter table Course add constraint PK_Course_CourseId primary key (CourseId);
-- 班级课程表 ClassCourseId 字段需要设置为主键（主键约束）
alter table ClassCourse add constraint PK_ClassCourse_ClassCourseId primary key (ClassCourseId);
-- 分数表 ScoreId 字段需要设置为主键（主键约束）
alter table Score add constraint PK_Score_ScoreId primary key (ScoreId);

-- 学生表 StudentName 不允许为空（非空约束）
alter table Student alter column StudentName nvarchar(50) not null;

-- 班级表 ClassName 需要唯一（唯一约束）
alter table Class add constraint UQ_Class_ClassName unique(ClassName);
-- 课程表 CourseName 需要唯一（唯一约束）
alter table Course add constraint UQ_Course_CourseName unique(CourseName);

-- 学生表 ClassId 增加默认值为0（默认值约束）
alter table Student add constraint DF_Student_ClassId default(0) for ClassId;

-- 学生表 StudentSex 只能为1或者2（Check约束）
alter table Student add constraint CK_Student_StudentSex check(StudentSex=1 or StudentSex=2 or StudentSex=3);
-- 分数表 Score 字段只能大于等于0（check约束）
alter table Score add constraint CK_Score_Score check(Score>=0);
-- 课程表 CourseCredit 字段只能大于0（check约束）
alter table Course add constraint CK_Course_CourseCredit check(CourseCredit>=0);

-- 班级课程表ClassId 对应是 班级表ClassId 的外键 （外键约束）
alter table ClassCourse add constraint FK_ClassCourse_ClassId foreign key (ClassId) references Class(ClassId);
-- 班级课程表CourseId 对应是 课程表CourseId 的外键 （外键约束）
alter table ClassCourse add constraint FK_ClassCourse_CourseId foreign key (CourseId) references Course(CourseId);
-- 分数表StudentId 对应是 学生表StudentId 的外键 （外键约束）
alter table Score add constraint FK_Score_StudentId foreign key (StudentId) references Student(StudentId);
-- 分数表CourseId 对应是 课程表CourseId 的外键 （外键约束）
alter table Score add constraint FK_Score_CourseId foreign key (CourseId) references Course(CourseId);


insert into Class(ClassName) values ('软件一班');
insert into Class(ClassName) values ('软件二班');
insert into Class(ClassName) values ('计算机应用技术班');
insert into Student(StudentName,StudentSex,StudentBirth,StudentAddress,ClassId)
	 values ('刘正','1','2000-01-01','广西省桂林市七星区空明西路10号鸾东小区','1')
	 ,('陈美','2','2000-07-08','福建省龙岩市新罗区曹溪街道万达小区','1')
insert into Student(StudentName,StudentSex,StudentBirth,StudentAddress,ClassId)
	values(' 江文','1','2000-08-10','安徽省合肥市庐阳区四里河路与潜山路交汇处万科城市之光','2')
	,('钟琪','2','2001-03-21','湖南省长沙市雨花区红花坡社区','2')
insert into Student(StudentName,StudentSex,StudentBirth,StudentAddress,ClassId)
	values('曾小林','1','1999-12-10','安徽省合肥市庐阳区四里河路与潜山路交汇处万科城市之光','3')
	,('欧阳天天','2','2000-04-05','湖北省武汉市洪山区友谊大道与二环线交汇处融侨悦府','3')
	,('徐长卿','1','2001-01-30','江苏省苏州市苏州工业园区独墅湖社区','3')
	,('李逍遥','1','1999-11-11','广东省广州市白云区金沙洲岛御洲三街恒大绿洲','3')
insert into Student(StudentName,StudentSex,StudentBirth,StudentAddress)
	values('东方不败','3','1999-12-11','河北省平定州西北四十余里的猩猩滩')
	,('令狐冲','1','2000-08-11','陕西省渭南市华阴市玉泉路南段')
insert into Course(CourseName,CourseCredit)
	 values('数据库高级应用','3')
	 ,('javascript编程基础','3')
	 ,('web前端程序设计基础','4')
	 ,('动态网页设计.net基础','6')
	

insert into ClassCourse(ClassId,CourseId)
	 values(1,1)
	 ,(1,2)
	 ,(1,3)
	 ,(1,4)
insert into ClassCourse(ClassId,CourseId)
	 values(2,1)
	 ,(2,2)
	 ,(2,3)
	 ,(2,4)
insert into Course(CourseName,CourseCredit)
	 values('动态网页设计php基础','6')
insert into ClassCourse(ClassId,CourseId)
	 values(3,1)
	 ,(3,2)
	 ,(3,3)
	 ,(3,5)
 select * from Course order by CourseCredit desc;
 select * from ClassCourse;
 select * from Class;
 select * from Student;
 select * from Score order by Score asc;
 select StudentName as 学生姓名,StudentBirth 学生生日 from Student
 select * from Student where ClassId=1;
 select * from Student where StudentBirth>='2000-01-01' and StudentBirth<='2001-01-01'
 select * from Student where StudentSex='2'
 create table stuinfo(
 StuNo nvarchar primary key,
 StuName nvarchar(50) not null,
 StuAge int,
 StuAddress nvarchar(255) not null,
 StuSeat int,
 StuSex tinyint,
 )
 insert into stuinfo(StuName,StuAge,StuAddress,StuSeat,StuSex)
	 values('s2501','张秋利','20','美国硅谷','1','1')
	 ,('s2502','李思文','18','湖北武汉','2','0')
	 ,('s2503','马文才','22','湖南长沙','3','1')
	 ,('s2504','欧阳俊雄','21','湖北武汉','4','0')
	 ,('s2505','梅超风','20','湖北武汉','5','1')
	 ,('s2506','陈玄风','19','美国硅谷','6','1')
	 ,('s2507','陈风','20','美国硅谷','7','0')
 create table stuexam(
 examNo int,
 StuNo nvarchar primary key,
 writtenExam nvarchar,
 labExam nvarchar,
 )
insert into stuexam(examNo,StuNo,writtenExam,labExam)
	 values('1','s2501','50','70')
	 ,('2','s2502','60','65')
	 ,('3','s2503','86','85')
	 ,('4','s2504','40','80')
	 ,('5','s2505','70','90')
	 ,('6','s2506','85','90')
 alter table stuinfo add constraint FK_stuinfo_StuNo foreign key(StuNo)
	 references stuexam(StuNo)
select * from stuinfo
select * from stuexam
select StuNo as 编号,StuName as 学生姓名,StuAge as 年龄,StuAddress 地址,StuSeat as 座位,stusex as 性别 from stuinfo
select StuName,StuAge,StuAddress from stuinfo;
select StuNo,writtenExam,labExam from stuexam;
select StuName+';'+StuAddress [姓名;地址]from stuinfo

