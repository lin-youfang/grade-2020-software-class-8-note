<?php
$searchList = [
    ["name" => "百度", "url" => "https://www.baidu.com", "score" => "70"],
    ["name" => "谷歌", "url" => "https://www.google.com", "score" => "90"],
    ["name" => "360搜索", "url" => "https://www.so.com", "score" => "62"],
    ["name" => "搜搜", "url" => "https://www.soso.com", "score" => "65"],
];
?>


<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
    <form action="demo02.php" method="get">
        <table border="1dx" cellspacing="0">
            <tr>
                <th><h3>序号</h3></th>
                <th><h3>网站名</h3></th>
                <th><h3>url地址</h3></th>
            </tr>
            <?php foreach ($searchList as $key => $value):?>
            <tr>
                <td><?php echo $key;?></td>
                <td><?php echo $value['name'];?></td>
                <td><?php echo $value['url'];?></td>
            </tr>
            <?php endforeach;?>
        </table>

    </form>

	</body>
</html>
