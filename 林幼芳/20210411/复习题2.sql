

---2、创建数据表HOUSE_DB
--房屋类型表（HOUSE_TYPE）
--字段名	类型	是否可为空	约束	说明
--type_id	int	N	主键，自增长	类型编号
--type_name	varchar(50)	N	唯一约束	类型名称

create database HOUSE_DB;
go
create table HOUSE_TYPE(
type_id int primary key identity(1,1),--类型编号
type_name	varchar(50) not null unique,--类型名称
)
go

--房屋信息表（HOUSE）
--字段名	类型	是否可为空	约束	说明
--house_id	int	N	主键，自增长	房屋编号
--house_name	varchar(50)	N		房屋名称
--house_price	float	N	默认值0	房租
--type_id	int	N	外键依赖HOUSE_TYPE表	房屋类型

create table HOUSE(
house_id int not null primary key identity(1,1),--房屋编号
house_name	varchar(50) not null,--房屋名称
house_price float not null default'0',--房租
type_id int foreign key (type_id) references HOUSE_TYPE(type_id),--房屋类型
)
go

--3、添加表数据
--HOUSE_TYPE表中添加3条数据，例如：小户型、经济型、别墅
--HOUSE表中添加至少3条数据，不能全都为同一类型
insert into HOUSE_TYPE(type_name) values('小户型')
insert into HOUSE_TYPE(type_name) values('经济型')
insert into HOUSE_TYPE(type_name) values('别墅')
go


--4、添加查询
--查询所有房屋信息
select * from HOUSE_TYPE

--使用模糊查询包含”型“字的房屋类型信息
select * from HOUSE_TYPE where  type_name like '%型%'

--查询出房屋的名称和租金，并且按照租金降序排序
select house_name,house_price from HOUSE order by house_price desc

--使用连接查询，查询信息，显示房屋名称和房屋类型名称
select house_name 房屋名称,type_name 房屋类型名称 from HOUSE
inner join HOUSE_TYPE on HOUSE.type_id=HOUSE_TYPE.type_id

--查询所有房屋中月租最高的房屋，显示最高的租金和房屋名称
select max(house_price),house_name from HOUSE
group by house_name

