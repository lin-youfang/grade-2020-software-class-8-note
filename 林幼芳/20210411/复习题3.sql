--1、创建明星数据库（StarManagerDB）,然后建立两张表，StarType（明星类型表），StarInfo（明星信息表），表结构分别如下：
--StarType（明星类型表）
--字段名	说明	类型	长度	可否为空	约束
--T_NO	明星类型编号	int		否	主键约束，自增，标识种子和标识增量都是1
--T_NAME	明星类型	nvarchar	20		
create database StarManagerDB;
use StarManagerDB;
create table StarType(
T_NO int not null primary key identity(1,1),--明星类型编号
T_NAME nvarchar(20),--明星类型

)


--StarInfo（明星信息表）
--字段名	说明	类型	长度	可否为空	约束
--S_NO	明星编号	int		否	主键约束，自增，标识种子和标识增量都是1
--S_NAME	明星姓名	nvarchar	20	否	
--S_AGE	明星年龄	int		否	
--S_HOBBY	特技	nvarchar	20		
--S_NATIVE	明星籍贯	nvarchar	20		默认约束：中国大陆
--S_T_NO	明星类型编号	int			外键，参照StarType表的主键T_NO

create table StarInfo(
S_NO int not null primary key identity(1,1),--明星编号
S_NAME nvarchar(20) not null,--明星姓名
S_AGE int not null,--明星年龄
S_HOBBY nvarchar(20),--特技
S_NATIVE nvarchar(20) default'中国大陆',--明星籍贯
S_T_NO int foreign key (S_T_NO) references StarType(T_NO),
)
--2、使用插入语句为两张表添加数据

--明星类型表（StarType） 
--明星类型编号	明星类型
--1	体育明星
--2	IT明星
--3	相声演员
insert  into StarType(T_NAME) values('体育明星')
insert  into StarType(T_NAME) values('IT明星')
insert  into StarType(T_NAME) values('相声演员')


--明星表（StarInfo）
--明星编号	姓名	年龄	特技	籍贯	明星类型编号
--1	梅西	30	射门	阿根廷	1
--2	科比	35	过人	美国	1
--3	蔡景现	40	敲代码	中国	2
--4	马斯克	36	造火箭	外星人	2
--5	郭德纲	50	相声	中国	3
--6	黄铮	41	拼多多	中国	2

insert into StarInfo(S_NAME,S_AGE,S_HOBBY,S_NATIVE) values('梅西','30','射门','阿根廷')
,('科比','35','过人','美国')
,('蔡景现','40','敲代码','中国')
,('马斯克','36','造火箭','外星人')
,('郭德纲','50','相声','中国')
,('黄峥','41','拼多多','中国')

--3、查询年龄最大的3个明星的姓名，特技和籍贯信息，要求使用别名显示列名。

select top 3 *,S_NAME 明星姓名,S_HOBBY 特技,S_NATIVE 籍贯 from StarInfo 

--4、按明星类型编号分类查询明星人数，明星平均年龄，显示明星人数大于2的分组信息，要求使用别名显示列名。

select S_NO,count(S_NO) 明星人数,avg(S_AGE) 平均年龄 from StarInfo
group by S_NO
having count(S_NO) >2

--5、查询明星类型为“体育明星”中年龄最大的姓名、特技、籍贯信息，要求显示列别名。

select max(S_AGE) ,StarInfo.S_NAME 姓名,S_HOBBY 特技,S_NATIVE 籍贯 from StarInfo
 inner join StarType on StarInfo.S_T_NO=StarType.T_NO
 group by StarInfo.S_NAME,S_HOBBY ,S_NATIVE

