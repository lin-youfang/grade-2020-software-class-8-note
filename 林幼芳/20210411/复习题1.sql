--1、创建商品数据库（GoodsDB），然后建立两张表，GoodsType（商品类型表），GoodsInfo（商品信息表），表结构分别如下：
--商品类型表（GoodsType）
--字段名	说明	类型	长度	可否为空	约束
--TypeID	商品类型编号	int		否	主键约束，自增约束，标识种子和标识增量都是1
--TypeName	商品类型名称	nvarchar	20	否	

create database GoodsDB;
use GoodsDB;
create table GoodType(
TypeID int not null primary key identity(1,1),
TypeName nvarchar(20) not null,
)

--商品信息表（GoodsInfo）
--字段名	说明	类型	长度	可否为空	约束
--GoodsID	商品编号	int		否	主键约束，自增约束，标识种子和标识增量都是1
--GoodsName	商品名称	nvarchar	20	否	
--GoodsColor	商品颜色	nvarchar	20	否	
--GoodsBrand	商品品牌	nvarchar	20		
--GoodsMoney	商品价格	money		否	
--TypeID	商品类型编号	int			外键，参照GoodsType中的TypeID

create table GoodsInfo(
GoodsId int not null primary key identity(1,1),--商品编号
GoodName nvarchar(20) not null,--商品名称
GoodColor nvarchar(20) not null,--商品颜色
GoodBrand nvarchar(20),--商品品牌
GoodMoney money not null,--商品价格
TypeID int,
)
alter table GoodsInfo add constraint  FK_GoodsInfo_TypeID foreign key(TypeID) references GoodType(TypeID);

--2、使用插入语句为两张表添加数据
--商品类型表（GoodsType）
--商品类型编号 商品类型名称
--1 服装内衣
--2 鞋包配饰
--3 手机数码

insert into GoodType(TypeName) values('服装内衣')
insert into GoodType(TypeName) values('鞋包服饰')
insert into GoodType(TypeName) values('手机数码')
go
select * from GoodType

--商品信息表（GoodsType）
--商品编号 商品名称 商品颜色 商品品牌 商品价格 商品类型编号
--1 提花小西装 红色 菲曼琪 300 1
--2 百搭短裤 绿色 哥弟 100 1
--3 无袖背心 白色 阿依莲 700 1
--4 低帮休闲鞋 红色 菲曼琪 900 2
--5 中跟单鞋 绿色 哥弟 400 2
--6 平底鞋 白色 阿依莲 200 2
--7 迷你照相机 红色 尼康 500 3
--8 硬盘 黑色 希捷 600 3
--9 显卡 黑色 技嘉 800 3
insert into GoodsInfo(GoodName,GoodColor,GoodBrand,GoodMoney,TypeID) 
	values('提花小西装','红色','费曼琪','300','1')
		 ,('百搭短裤','绿色','哥弟','100','1')
		 ,('无袖背心','白色','阿依莲','700','1')
		 ,('低帮休闲鞋','红色','菲曼琪','900','2')
		 ,('中跟单鞋','绿色','哥弟','400','2')
		 ,('平底鞋','白色','阿依莲','200','2')
		 ,('迷你照相机','红色','尼康','500','3')
		 ,('硬盘','黑色','希捷','600','3')
		 ,('显卡','黑色','技嘉','800','3')

select * from GoodsInfo 
--3、查询价格最贵的商品名称，商品颜色和商品价格，要求使用别名显示列名
select top 1 *,GoodName 商品名称,GoodColor 商品颜色,GoodMoney 商品价格 from GoodsInfo order by GoodName desc 

--4、按商品类型编号分组查询商品最高价格，最低价格和平均价格，要求使用别名显示列名
select GoodsId, max(GoodMoney) 最高价格 ,min(GoodMoney) 最低价格,avg(GoodMoney) 平均价格 from GoodsInfo
group by GoodsId 

--5、查询商品信息所有列，要求商品颜色为红色，价格在300~600之间
select * from GoodsInfo where GoodColor='红色' and GoodMoney in(300,600)