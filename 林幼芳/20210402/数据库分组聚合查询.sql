create database ordeInfo;
go

--使用数据库
use ordeInfo;
go


--创建订单表
create table orders(
orderId int primary key identity(1,1),	--订单编号,主键，标识列（自增）
orderDate datetime	 --订购日期
);
go

--创建订购项目表
create table orderltem(
ltemiD int primary key identity(1,1),	--项目编号，主键，标识列（自增）
orderID int references orders(orderId), --订单编号,外键
itemType nvarchar(10), --产品类别
itemName nvarchar(10),--产品名称
theNumber int,-- 订购数量
theMoney int -- 订购单价
);
go
insert into orders(orderDate) values('2008-01-12');
insert into orders(orderDate) values('2008-02-10');
insert into orders(orderDate) values('2008-02-15');
insert into orders(orderDate) values('2008-03-10')
go


--为订购项目表插入数据
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(1,'文具','笔',72,2);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(1,'文具','尺',10,1);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(1,'体育用品','篮球',1,56);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(2,'文具','笔',36,2);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(2,'文具','固体胶',20,3);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(2,'日常用品','透明胶',2,1);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(2,'体育用品','羽毛球',20,3);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(3,'文具','订书机',20,3);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(3,'文具','订书针',10,3);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(3,'文具','裁纸刀',5,5);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(4,'文具','笔',20,2);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(4,'文具','信纸',50,1);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(4,'日常用品','毛巾',4,5);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(4,'日常用品','透明胶',30,1);
insert into orderltem(orderID,itemType,itemName,theNumber,theMoney) values(4,'体育用品','羽毛球',20,3);
go
--1.查询所有订单订购的所有物品数量总和
select sum(theNumber) 所有订单订购的所有物品数量总和 from orderltem
--2.查询订单编号小于3的，平均单价小于10 每个订单订购的所有物品的数量和以及平均单价
select orderID,  avg(theMoney),sum(theNumber) from orderltem where orderID<3
group by orderID
having avg(theMoney)<10
--3.查询平均单价小于10并且总数量大于 50 每个订单订购的所有物品数量和以及平均单价
select orderID , avg (theMoney) ,sum (theNumber)from orderltem
group by orderID
having avg(theMoney)<10 and sum(theNumber)>50
--4.查询每种类别的产品分别订购了几次，例如：
		          --文具      9
                          -- 体育用品  3
                                         -- 日常用品  3
select itemType, count(itemType)from  orderltem
group by itemType


--5.查询每种类别的产品的订购总数量在100以上的订购总数量和平均单价
select itemType,sum(theNumber)订购总数量,avg(theMoney) from orderltem
group by itemType
having sum(theNumber)>100
--6.查询每种产品的订购次数，订购总数量和订购的平均单价，例如：

  --产品名称   订购次数  总数量   平均单价 
    --笔           3       120       2
select itemName,count(orderID),sum(theNumber),avg(theMoney) from orderltem
group by itemName

