﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo03
{
  static  class SumUtil
    {
        //        2.创建一个名为计算工具类 SumUtils，在类中定义4个方法：
        //   	计算两个整数相加、
        //   	两个小数相加、 
        //   	两个字符串相加、
        //	以及从 1 到指定整数的和的方法。
        //在 Main 方法中分别调用定义好的方法。

        public static int Sum(int a, int b) {
            return a + b;
        }
        public static double Sum(double c, double d)
        {
            return c  +d;
        }
        public static String Sum(string e, string f)
        {
            return e + f;
        }

    }
}
