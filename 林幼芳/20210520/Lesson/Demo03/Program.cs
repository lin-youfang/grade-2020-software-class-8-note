﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo03
{
    class Program
    {
        static void Main(string[] args)
        {
            //   2.创建一个名为计算工具类 SumUtils，在类中定义4个方法：
            //   计算两个整数相加、
            //   两个小数相加、 
            //   两个字符串相加、
            //以及从 1 到指定整数的和的方法。
            //   在 Main 方法中分别调用定义好的方法。
            int a = 2;
            int b = 3;
            Console.WriteLine(a+"+"+b+"="+SumUtil.Sum(a,b));
            double c = 3.5;
            double d = 2.7;
            Console.WriteLine(c + "+" + d + "=" + SumUtil.Sum(c, d));
            string e = "2356";
            string f = "123";
            Console.WriteLine(e + "+" + f + "=" + SumUtil.Sum(e, f));


        }
    }
}
