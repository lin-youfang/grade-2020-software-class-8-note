﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo6
{
    class Program
    {
        static void Main(string[] args)
        {
            //现在开班了，班里老师只有一个，叫唐僧。班里的五个人都知道老师叫唐僧(也就是说老师这个字段是共享的)。
            //但是某一天唐僧要去女儿国走丈母娘家了，课不能停啊。所以就请嫦娥姐姐过来带一段课。

            //现在5个学生都知道老师换成嫦娥姐姐了，都很激动哈。但是她们是怎么都知道换老师的呢？还是这个静态字段的功劳
            //将老师姓名声明为静态字段，这个字段是共享的。所以该类的属性都能知道。
            Student  student=new Student();
            student.StuName="孙悟空";
            Student student1=new Student();
            student1.StuName="沙和尚";
            Student student2=new Student();
            student2.StuName = "猪八戒";
            Student.teacher="唐僧";
            Student.teacher="嫦娥姐姐";
            student.Print();
            Console.ReadKey();

                






        }
    }
}
