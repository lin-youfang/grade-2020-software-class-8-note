﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class Program
    {
        static void Main(string[] args)
        {

            //            定义一个员工类，存放用户的工号、姓名、性别、学历和部门信息；
            //定义两个构造函数:
            //            一个是无参构造函数，学历默认为专科；
            //一个有参构造函数，根据参数对类的属性进行初始化。
            Staff staff = new Staff("001","卢晓丽","女","设计部");
            Staff.Education = "大专";

            staff.Print();


        }
    }
}
