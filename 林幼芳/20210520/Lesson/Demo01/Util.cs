﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class Staff
    {
        //定义一个员工类，存放用户的工号、姓名、性别、学历和部门信息；
        //定义两个构造函数:
        //一个是无参构造函数，学历默认为专科；
        //一个有参构造函数，根据参数对类的属性进行初始化。

       private string id;
       private string name;
       private string sex;
       public static string Education { get; set; }
       private string department;

        public Staff(string id,string name,string sex, string department) {
            this.id = id;
            this.name = name;
            this.sex = sex;
            this.department = department;
        }
        public void Print() {
            Console.WriteLine("工号："+this.id);
            Console.WriteLine("姓名："+this.name);
            Console.WriteLine("性别:"+this.sex);
            Console.WriteLine("学历："+Education);
            Console.WriteLine("部门："+this.department);
        
        }

    }
}
