﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Program
    {
        static void Main(string[] args)
        {
            //2. 定义一个学生类，存放学生的学号、姓名、性别、年龄、专业信息；
            // 对年龄字段进行赋值的安全性设置，如果是非法值（小于0或者大于128岁），该年龄值为0；
            // 在学生类中定义一个方法输出学生信息。
            //在主方法实例化对象，赋值并输出
            Student student = new Student();
            student.id = "001";
            student.name = "林幼芳";
            student.Sex = "女";
            student.Age = 20;
            student.major = "软件技术";
            student.Print();



        }
       
    }
}
