﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Student
    {//学号、姓名、性别、年龄、专业
        public string id;
        public string name;
        private string sex;
        private int age;
        public string major;
        public void Print()
        {
            Console.WriteLine("学号：" + this.id);
            Console.WriteLine("姓名：" + this.name);
            Console.WriteLine("性别：" + this.Sex);
            Console.WriteLine("年龄：" + this.Age);
            Console.WriteLine("专业" + this.major);
        }
        public string Sex
        {
            get
            {
                return sex;
            
            }
            set
            {
                if (value.Equals("男") || value.Equals("女")) {
                    sex = value;
                }

            }
           
        
        }
        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                if (value > 0 && value < 128)
                {
                    age = value;
                }
            }

        }


        }

    }

