﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo3
{
    class Program
    {
        static void Main(string[] args)
        {
            //3.定义一个图书类，存放图书的编号、书名、价格、出版社、作者信息；
            //对价格进行赋值限制，小于0价格，赋值为0
            //在图书类中定义一个方法输出图书信息；
            //在主方法实例化对象，赋值并输出
            Book book = new Book();
            book.bookid = "00001";
            book.bookname = "斗罗大陆";
            book.Price = 35.5;
            book.pubcom = "湖北";
            book.author = "唐三";
            book.Print();
            book.bookid = "00002";
            book.bookname = "数据库";
            book.Price = 35.5;
            book.pubcom = "湖北";
            book.author = "里斯";
            book.Print();


        }
    }
}
