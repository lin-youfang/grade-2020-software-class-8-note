﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo3
{
    class Book
    {
        //存放图书的编号、书名、价格、出版社、作者信息
       public string bookid;
       public string bookname;
       private double price;
       public string pubcom;
       public string author;
        public void Print() {
            Console.WriteLine("编号："+this.bookid);
            Console.WriteLine("书名"+this.bookname);
            Console.WriteLine("价格"+this.price);
            Console.WriteLine("出版社"+this.pubcom);
            Console.WriteLine("作者"+this.author);
        }
        public double Price {
            get
            {
                return price;

            }
            set {
                if (value > 0)
                {
                    price = value;
                }
                else { 
                }
            }
        }
    }
}
