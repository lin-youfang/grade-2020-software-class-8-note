import java.util.Scanner;

public class Demo01 {
	//接收并输出某公司本月的考勤和扣款信息，
	//假设公司有5个员工， 每个员工的考勤项有上下班忘打卡、 迟到、 早退、 旷工， 其中上下班忘打卡扣款10元/次， 
	//迟到和早退扣款为20元/次， 旷工100元/天，
	public static void main(String[] args) {
	Scanner s=new Scanner(System.in);
	int [][] kq=new int[2][5];
	
	for (int i = 0; i < kq.length; i++) {

			System.out.println("请输入员工编号");
			kq[i][0]=s.nextInt();
			System.out.println("请输入第"+(i+1)+"位员工忘打卡的次数");
			kq[i][1]=s.nextInt();
			System.out.println("请输入第"+(i+1)+"位员工迟到的次数");
			kq[i][2]=s.nextInt();
			System.out.println("请输入第"+(i+1)+"位员工早退的次数");
			kq[i][3]=s.nextInt();
			System.out.println("请输入第"+(i+1)+"位员工旷工的次数");
			kq[i][4]=s.nextInt();
		
	}
	System.out.println("************本月考勤信息*************");
	System.out.println("编号\t忘打卡\t迟到\t早退\t旷工\t总罚款");
	for (int i = 0; i < kq.length; i++) {
		for (int j = 0; j < kq[i].length; j++) {
		System.out.print(kq[i][j]+"   ");
		
		}
		
		System.out.println(kq[i][1]*10+kq[i][2]*20+kq[i][4]*100);
		
		
	}
		
	
	}

}
