package jpg;
import java.util.Scanner;
public class demo04 {

	public static void main(String[] args) {
		//顾客从超市采购了10件商品，编写一个程序，用于接受每件商品的价格，
		//计算应付的总金额。并分别打印出各个商品的价格以及应付的总金额。
		String a[]= {"鸡蛋","牛奶","饼干","矿泉水","牙膏","鱼","杯子","洗发水","沐浴露","牙刷"};
		Scanner s=new Scanner(System.in);
		double b[]=new double[10];
		for(int i=0;i<a.length;i++) {
			System.out.println("请输入"+a[i]+"价格");
			b[i]=s.nextDouble();
		}
		double sum=0;
		for(int i=0;i<a.length;i++) {
			System.out.println(a[i]+"的价格为"+b[i]+"元");
			sum=sum+b[i];
		}
		System.out.println("总金额为"+sum);
		
	}

}
