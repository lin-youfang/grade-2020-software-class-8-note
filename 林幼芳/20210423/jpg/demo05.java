package jpg;

public class demo05 {

	public static void main(String[] args) {
	//将一个数组中的元素逆序输出，即第一个元素和最后一个元素交换，
	//第二个数与倒数第二元素交换…..，例如：原数组为：9  2  5  7   8，逆序后的数组为：8   7   5  2  9
	int []arr= {7,2,5,9,8};
	int temp;
	for(int i=0;i<arr.length/2;i++) {
		temp=arr[i];
		arr[i]=arr[arr.length-i-1];
		arr[arr.length-i-1]=temp;
				
		}
		for(int i : arr) {
			if(i==arr.length-i-1) {
				System.out.print(i);
			}else {
				System.out.print(i+" ");
				}
	}
	}
}
