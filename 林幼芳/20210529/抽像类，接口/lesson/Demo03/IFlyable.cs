﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo03
{
    interface IFlyable
    {
       IFlyable TakeOff();
        IFlyable Fly();
        IFlyable Land();

    }
}
