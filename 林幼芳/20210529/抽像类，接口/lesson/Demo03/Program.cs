﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo03
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.飞机是交通工具类，有运输载人的功能；
            //2.小鸟和超人都是动物类，都有吃的方法；小鸟有自己的特有方法，下蛋；
            //3.超人、小鸟、飞机都有飞的功能，可以定义飞的接口;
            //飞的接口有起飞、飞行中、着陆的方法；
            //4.超人、小鸟、飞机除了继承各自的父类后，还要继承飞的接口，实现飞的接口的方法；
            SuperMan superMan = new SuperMan();
            superMan.Fly();
            Bird bird = new Bird();
            bird.Land();



        }
    }
}
