﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class Skill
    {
        // 一、猫、狗、鸭、猴，（吃、游泳、爬树）
        //所有动物都有吃的方法
        //狗和鸭会游泳，不会爬树
        //猫和猴不会游泳会爬树
        //将吃的方法定义在父类方法中，将游泳和爬树的技能定义为借口
        //所有子类继承父类后，再去继承相应的接口实现技能
        private string name;
        private string eat;
        
        public string Name { get => name; set => name = value; }
        public string Eat { get => eat; set => eat = value; }

        public void GetEat() { 
        
        }


    }
}
