﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo02
{
    class Program
    {
        static void Main(string[] args)
        {
            // 二、蝙蝠战车的例子
            //定义一个父类表示车的共同属性特征行为：品牌名Brand，会跑Run方法
            //定义一个飞的功能接口类IFly：定义飞的方法Fly
            //然后定义蝙蝠战车类继承Cat类和飞的接口
            //在主方法实例化蝙蝠战车的对象，并为品牌名赋值，调用跑和飞的方法
            BatCar batCar = new BatCar("迅速","蝙蝠战车");

        }
    }
}
