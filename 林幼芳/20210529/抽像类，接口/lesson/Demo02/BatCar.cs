﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo02
{
    class BatCar : Car, IFly
    {
        private string fly;

        public string Fly { get => this.fly; set => this.fly = value; }

        public new void GetRun()
        {
            Console.WriteLine("");
        }

        public  BatCar(string fly,string brand):base(brand) {
            this.fly = fly;
        }
        string IFly.Fly()
        {
            return fly;
        }
    }
}
