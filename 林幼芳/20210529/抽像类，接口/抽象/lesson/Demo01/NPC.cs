﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
  abstract  class NPC
    {
        //分析：任务 NPC，商贩 NPC，铁匠 NPC，三种 NPC 的种类。

        //共有属性：npc 的名字，npc 的类型；

        //共有方法：都能和玩家交互(交谈)；
        private string name;
        private string type;

        protected string Name { get => name; set => name = value; }
        protected string Type { get => type; set => type = value; }
        public NPC(string name,string type) {
            this.name = name;
            this.type = type;
        }

        public abstract void Talk();
        
        
    }
}
