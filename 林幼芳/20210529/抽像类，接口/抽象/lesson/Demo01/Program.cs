﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class Program
    {
        static void Main(string[] args)
        {
            //2.使用抽象类结构实现游戏中NPC 模块

            //在游戏中会出现很多种不同用途的 NPC，这些 NPC 有各自的存在的价值和作用
            //，同时又具备一些共性的东西。在开发 NPC 系统的时候，往往是需要提取共性，独立出一个父类，然后子类继承实现不同作用的 NPC。

            //分析：任务 NPC，商贩 NPC，铁匠 NPC，三种 NPC 的种类。

            //共有属性：npc 的名字，npc 的类型；

            //共有方法：都能和玩家交互(交谈)；
            WorkNPC workNPC = new WorkNPC("小丸子","任务类型");
            workNPC.Talk();

        }
    }
}
