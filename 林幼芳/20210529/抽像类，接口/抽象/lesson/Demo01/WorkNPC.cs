﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class WorkNPC:NPC
    {
        public WorkNPC(string name,string type):base(name,type) { 
        
        }
        public override void Talk()
        {
            Console.WriteLine(Name+Type+"的NPC正在交谈");
        }
    }
}
