﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            // 添加3个类，分别实现 IComparer接口，实现对Student类的三个字段的排序。
            //1、学生类：学号、姓名、年龄
            //2、请选择：1、添加学生信息。2、删除学生信息 2、查询学生信息。
            //3、重复的学号不能添加。
            //4、查询学生信息功能中有：1、查询所有（按学号排序）2、查询所有（按姓名排序），2、查询所有（按年龄排序）4、按学号查询（查没有，则打印查无此学生）5、退出

            


            Student stu1 = new Student("001","黄月婷",19);

            Student stu2 = new Student("002","陈丽悦",20);

            Dictionary<string,Student> dc = new Dictionary<string,Student>();
            dc.Add(stu1.Num,stu1);
            dc.Add(stu2.Num,stu2);

            while (true)
            {
                Console.WriteLine("请选择：1.添加学生信息 2.删除学生信息 3.查询学生信息4.退出");
                int xz = int.Parse(Console.ReadLine());
                switch (xz)
                {
                    case 1:
                        //添加学生信息
                        AddStudent(dc);
                        Print(dc);
                        break;
                    case 2:
                        //删除学生信息
                        DeleteStudent(dc);
                        Print(dc);
                        break;
                    case 3:
                        //查询学生信息
                        QueryStudent(dc);
                        break;
                    case 4:
                        //退出
                        break;
                    default:
                       
                        break;
                }
               
            }
           


        }

        private static void QueryStudent(Dictionary<string, Student> dc)
        {
            // 1、查询所有（按学号排序）2、查询所有（按姓名排序），2、查询所有（按年龄排序）4、按学号查询
            Console.WriteLine("请输入要查询的学生信息：1、查询所有（按学号排序）2、查询所有（按姓名排序），3、查询所有（按年龄排序）4.按学号查询如果没有打印此学生 5.退出");
           int cx=int.Parse(Console.ReadLine());

            switch (cx)
	        {
                    case 1:
                    //按学号查询
                    NumSort(dc);
                        break;
                    case 2:
                    //按姓名查询
                    NameSort(dc);
                        break;
                    case 3:
                    //按年龄查询
                    AgeSort(dc);
                        break;
                    case 4:
                    //按学号查询如果没有打印此学生
                    QueryByNum(dc);
                        break;
                    case 5:
                    //退出
                        break;
		        default:
         break;
	        }


      
}

        private static void QueryByNum(Dictionary<string, Student> dc)
        {
            //按学号查询如果没有打印此学生
            Console.WriteLine("请输入学号：");
            string num = Console.ReadLine();
            if (dc.ContainsKey(num)) {

                Console.WriteLine(dc[num]);
            }
            else{

                Console.WriteLine("没有找到该学生信息");
            }

        }

        private static void AgeSort(Dictionary<string, Student> dc)
        {
            List<Student> students = new List<Student>();
            students.AddRange(dc.Values);
            students.Sort(new AgeSort());
            Print(students);

        }

        private static void NameSort(Dictionary<string, Student> dc)
        {
            List<Student> students = new List<Student>();
            students.AddRange(dc.Values);
            students.Sort(new NameSort());
            Print(students);

        }

        private static void NumSort(Dictionary<string, Student> dc)
        {
            List<Student> students = new List<Student>();
            students.AddRange(dc.Values);
            students.Sort(new NumSort());
            Print(students);
        }

        private static void Print(List<Student> students)
        {
            foreach (var item in students)
            {
                Console.WriteLine(item);
            }
        }

        private static void DeleteStudent(Dictionary<string, Student> dc)
        {
            Console.WriteLine("请输入要删除的学号：");
            string num = Console.ReadLine();
            if (dc.ContainsKey(num))
            {
                dc.Remove(num);
                Console.WriteLine("删除成功");
            }
            else {
                Console.WriteLine("该学生信息不存在，无法删除");
            }
        }

        private static void Print(Dictionary<string, Student> dc)
        {
            foreach (var item in dc.Values)
            {
                Console.WriteLine(item);
            }
        }

        private static void AddStudent(Dictionary<string,Student> dc)
        {
            string flag = null;

            while (true)
            {
                Console.WriteLine("请输入要添加的学生学号：");
                string num = Console.ReadLine();
                Console.WriteLine("请输入要添加的学生姓名：");
                string name = Console.ReadLine();
                Console.WriteLine("请输入要添加的学生年龄");
                int age = int.Parse(Console.ReadLine());

                Student student = new Student(num, name, age);

                if (!dc.ContainsKey(num))
                {
                    dc.Add(student.Num, student);
                    Console.WriteLine("添加成功");
                }
                else
                {
                    Console.WriteLine("已存在添加失败");
                }
                Console.WriteLine("是否继续添加：Yes/No");
                flag = Console.ReadLine();
                if (!flag.Equals("yes")) {

                    break;

                }
            }
           



        }
    }
}
