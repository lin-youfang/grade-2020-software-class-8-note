use Student;
go
select * from Student where StudentName like '欧阳%'
select * from Student where StudentAddress like '%桂林市%'
select * from Student where StudentName like '李__'
select * from Student where StudentName like '____'
select  count(*) 软件1班学生总数 from Student where ClassId=1;
select count(*) 软件1班课程数 from ClassCourse where ClassId=1;
select sum(Score) 李逍遥学习课程总学分 from score where StudentId=9
select sum(Score) 刘正学习课程总分 from score where StudentId=1;
select avg(Score) 平均分 from Score where courseid=1;
select sum(score) 总分 from Score;
select avg(Score) 平均分 from Score;



create table stuinfo(
	stuNO nvarchar(50) primary key,
	stuName nvarchar(50) not null,
	stuAge int not null,
	stuAddress nvarchar(225) not null,
	stuSeat int not null,
	stuSex int not null
);
insert into stuinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,stuSex)
	values('s2501','张秋利',20,'美国硅谷',1,1)
	,('s2502','李斯文',18,'湖北武汉',2,0)
	,('s2503','马文才',22,'湖南长沙',3,1)
	,('s2504','欧阳俊雄',21,'湖北武汉',4,0)
	,('s2505','梅超风',20,'湖北武汉',5,1)
	,('s2506','陈旋风',19,'美国硅谷',6,1)
	,('s2507','陈风',20,'美国硅谷',7,0)

--查询学生信息表（stuinfo）中所有列信息，给每列取上中文名称
select 学号=stuNO,姓名=stuName,年龄=stuAge,家庭地址=stuAddress,座号=stuSeat,性别=stuSex from stuinfo
--查询学生信息表（stuinfo）中的姓名，年龄和地址三列的信息
select stuName 姓名,stuAge 年龄,stuAddress 地址 from stuinfo
select * from stuinfo

--创建学生分数表stuexam
create table stuexam(
	examNO int  identity(1,1),
	stuNO nvarchar(50),
	writtenExam int not null,
	labExam int not null
);
insert into stuexam(stuNO,writtenExam,labExam)
	values('s2501',50,70)
	,('s2502',60,65)
	,('s2503',86,85)
	,('s2504',40,80)
	,('s2505',70,90)
	,('s2506',85,90);
alter table stuexam add constraint FK_stuexam_StuNO foreign key(stuNO) references stuinfo(stuNO);
--查询学生分数表（stuexam）中的学号，笔试和机试三列的信息，并为这三列取中文名字
--注意：要用三种方法
select stuNO 学号, writtenExam 笔试,labExam 机试 from stuexam;
select stuNO as 学号, writtenExam as 笔试,labExam as 机试 from stuexam;
select 学号=stuNO, 笔试=writtenExam,机试=labExam from stuexam;
--查询学生信息表（stuInfo）中的学号，姓名，地址，以及将：姓名+@+地址 组成新列 “邮箱”
select stuNo 学号, stuName 学生姓名, stuAddress 家庭地址, stuName+'@'+stuAddress 邮箱 from stuinfo;
--5.查询学生分数表（stuexam）中的学生的学号，笔试，机试以及总分这四列的信息
select stuNO,writtenExam,labExam from stuexam;
select * from stuexam;
select top 3* from stuinfo;
select top 4 stuName,stuSeat  from stuinfo;
select top 50 percent * from stuinfo;
select * from stuinfo where stuAddress like '%湖北武汉%' and stuAge like'20';
select * from stuexam where labExam between '60' and '80';
select * from stuexam where labExam>=60 and labExam<=80;
select * from stuinfo where stuAddress='湖北武汉' or stuAddress='湖南长沙'
select * from stuinfo where stuAddress in('湖北武汉','湖南长沙')
select * from stuinfo where stuAge is null;
select * from stuinfo where stuAge is not null;
select * from stuinfo where stuName like '张%';
select * from stuinfo where stuAddress like '%湖%'
select * from stuinfo where stuName like '张_'
select * from stuinfo where stuName like '__俊%'
select * from stuinfo order by stuAge asc;
select * from stuinfo order by stuAge,stuSeat desc;
select max(writtenExam) from stuexam;
select * from stuinfo;
select * from stuexam;
  