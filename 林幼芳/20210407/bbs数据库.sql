use master
go

--判断数据库是否存在
if exists(select * from sys.databases where name='bbs')
begin 
	drop database bbs  --存在删库
end
go


--创建数据库
create database bbs
go

--使用数据库
use bbs
go
	

--创建用户信息表（bbsUsers）
create table bbsUsers
(
	UID int identity(1,1) primary key,	--用户编号  UID int 主键  标识列
	uName varchar(10) unique not null,	--用户名    uName varchar(10)  唯一约束 不能为空
	uSex  varchar(2) check(uSex='男' or uSex='女') not null,	--性别      uSex  varchar(2)  不能为空 只能是男或女
	uAge  int not null check(uAge>=15 and uAge<=60),	--年龄      uAge  int  不能为空 范围15-60
	uPoint  int not null  check(uPoint>=0)	--积分      uPoint  int 不能为空  范围 >= 0
)
go


--创建版块表（bbsSection）
create table bbsSection
(
	sID  int identity(1,1) primary key,	--版块编号  sID  int 标识列 主键
	sName  varchar(10) not null,	--版块名称  sName  varchar(10)  不能为空
	sUid   int references bbsUsers(UID)	--版主编号  sUid   int 外键  引用用户信息表的用户编号
)
go


--创建主贴表（bbsTopic）	
create table bbsTopic
(
	tID  int identity(1,1) primary key,	--主贴编号  tID  int 主键  标识列
	tUID  int references bbsUsers(UID),	--发帖人编号  tUID  int 外键  引用用户信息表的用户编号
	tSID  int references bbsSection(sID),	--版块编号    tSID  int 外键  引用版块表的版块编号    （标明该贴子属于哪个版块）
	tTitle  varchar(100) not null,	--贴子的标题  tTitle  varchar(100) 不能为空
	tMsg  nvarchar(max) not null,	--帖子的内容  tMsg  nvarchar(max)  不能为空
	tTime  datetime,	--发帖时间    tTime  datetime  
	tCount  int	--回复数量    tCount  int
)
go
 

--创建回帖表（truncate table）
create table bbsReply
(
	rID  int identity(1,1) primary key,--回贴编号  rID  int 主键  标识列，
	rUID  int references bbsUsers(UID),--回帖人编号  rUID  int 外键  引用用户信息表的用户编号
	rTID  int references bbsTopic(tID),--对应主贴编号    rTID  int 外键  引用主贴表的主贴编号    （标明该贴子属于哪个主贴）
	rMsg  nvarchar(max) not null,--回帖的内容  rMsg  nvarchar(max)  不能为空
	rTime  datetime,--回帖时间    rTime  datetime 
)
go
