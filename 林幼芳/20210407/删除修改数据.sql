use master
go

--判断数据库是否存在
if exists(select * from sys.databases where name='bbs')
begin 
	drop database bbs  --存在删库
end
go


--创建数据库
create database bbs
go

--使用数据库
use bbs
go
	

--创建用户信息表（bbsUsers）
create table bbsUsers
(
	UID int identity(1,1) primary key,	--用户编号  UID int 主键  标识列
	uName varchar(10) unique not null,	--用户名    uName varchar(10)  唯一约束 不能为空
	uSex  varchar(2) check(uSex='男' or uSex='女') not null,	--性别      uSex  varchar(2)  不能为空 只能是男或女
	uAge  int not null check(uAge>=15 and uAge<=60),	--年龄      uAge  int  不能为空 范围15-60
	uPoint  int not null  check(uPoint>=0)	--积分      uPoint  int 不能为空  范围 >= 0
)
go


--创建版块表（bbsSection）
create table bbsSection
(
	sID  int identity(1,1) primary key,	--版块编号  sID  int 标识列 主键
	sName  varchar(10) not null,	--版块名称  sName  varchar(10)  不能为空
	sUid   int references bbsUsers(UID)	--版主编号  sUid   int 外键  引用用户信息表的用户编号
)
go


--创建主贴表（bbsTopic）	
create table bbsTopic
(
	tID  int identity(1,1) primary key,	--主贴编号  tID  int 主键  标识列
	tUID  int references bbsUsers(UID),	--发帖人编号  tUID  int 外键  引用用户信息表的用户编号
	tSID  int references bbsSection(sID),	--版块编号    tSID  int 外键  引用版块表的版块编号    （标明该贴子属于哪个版块）
	tTitle  varchar(100) not null,	--贴子的标题  tTitle  varchar(100) 不能为空
	tMsg  nvarchar(max) not null,	--帖子的内容  tMsg  nvarchar(max)  不能为空
	tTime  datetime,	--发帖时间    tTime  datetime  
	tCount  int	--回复数量    tCount  int
)
go
 

--创建回帖表（truncate table）
create table bbsReply
(
	rID  int identity(1,1) primary key,--回贴编号  rID  int 主键  标识列，
	rUID  int references bbsUsers(UID),--回帖人编号  rUID  int 外键  引用用户信息表的用户编号
	rTID  int references bbsTopic(tID),--对应主贴编号    rTID  int 外键  引用主贴表的主贴编号    （标明该贴子属于哪个主贴）
	rMsg  nvarchar(max) not null,--回帖的内容  rMsg  nvarchar(max)  不能为空
	rTime  datetime,--回帖时间    rTime  datetime 
)
go
--1.现在有3个会员注册成功，请用一次插入多行数据的方法向bbsUsers表种插入3行记录，记录值如下：
--	  小雨点  女  20  0
--	  逍遥    男  18  4
--	  七年级生  男  19  2
 insert into bbsUsers(uName,uSex,uAge,uPoint)
	 values('小雨点','女','20','0')
		   ,('逍遥','男','18','4')
		   ,('七年级生','男','19','2')
select * from bbsUsers
--	3.给论坛开设4个板块
--	  名称        版主名
--	  技术交流    小雨点
--	  读书世界    七年级生
--	  生活百科     小雨点
--	  八卦区       七年级生

insert into bbsSection(sName,sUid) values('技术交流','1')
insert into bbsSection(sName,sUid) values('读书世界','3')
insert into bbsSection(sName,sUid) values('生活百科','1')
insert into bbsSection(sName,sUid) values('八卦区','3')
select * from bbsSection


--	4.向主贴和回帖表中添加几条记录
	  
--	   主贴：

--	  发帖人    板块名    帖子标题                帖子内容                发帖时间   回复数量
--	  逍遥      八卦区     范跑跑                 谁是范跑跑              2008-7-8   1
--	  七年级生  技术交流   .NET                   与JAVA的区别是什么呀？  2008-9-1   2
--	  小雨点   生活百科    今年夏天最流行什么     有谁知道今年夏天最流行  2008-9-10  0
--						      什么呀？
select * from bbsUsers
select * from bbsTopic
insert into  bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) 
	values('2','4','范跑跑','谁是范跑跑','2008-7-8','1')
		 ,('3','1','.NET','与JAVA的区别是什么呀？','2008-9-1 ','2')
		 ,('1','3','今年夏天最流行什么呀？','有谁知道今年夏天最流行什么','2008-9-10','0')


--	   回帖：
--	   分别给上面三个主贴添加对应的回帖，回帖的内容，时间，回帖人自定

insert into bbsReply(rMsg,rTime,rUID) values('唐九洲公演排名','2021-04-07','1')
insert into bbsReply(rMsg,rTime,rUID) values('唐九洲冷笑话','2021-04-07','3')
insert into bbsReply(rMsg,rTime,rUID) values('十周','2021-04-07','2')
select * from bbsReply

--	5.因为会员“逍遥”发表了非法帖子，现将其从论坛中除掉，即删除该用户，请用语句实现（注意主外键，要删除主键，先要将引用了该主键的外键数据行删除）
delete from bbsUsers where uName='逍遥'
--	6.因为小雨点发帖较多，将其积分增加10分
update bbsUsers set uPoint=uPoint+10 where uName='小雨点'

--	7.因为板块“生活百科”灌水的人太少，现决定取消该板块，即删除（注意主外键）
delete from bbsSection where sName='生活百科'
--	8.因回帖积累太多，现需要将所有的回帖删除
delete from bbsReply 



  