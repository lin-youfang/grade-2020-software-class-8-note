﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo8
{
    class Program
    {
        static void Main(string[] args)
        {
            //7.	定义一个方法，实现一维数组的排序功能，从大到小排序。
            int []a={ 3, 5, 6, 7, 8 };
            Array.Reverse(a);
            foreach (var item in a)
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();
        }
    }
}
