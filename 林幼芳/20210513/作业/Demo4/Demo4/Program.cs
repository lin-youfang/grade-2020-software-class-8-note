﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo4
{
    class Program
    {
        static void Main(string[] args)
        {
            //3.	用户输入三个数，找出最大的数，打印输出。
            Console.WriteLine("请输入三个数");
           
            double max = 0.0;
            double min = 0.0;
            for (int i = 1; i <= 3; i++) {
                Console.WriteLine("请输入第" + i + "个数字");
                double c = double.Parse(Console.ReadLine());
                if (max < c)
                {
                    max = c;
                }
                else if (min > c)
                {
                    min = c;
                }

            }

            Console.WriteLine(max);
            Console.WriteLine(min);
            Console.ReadKey();

        }
    }
}
