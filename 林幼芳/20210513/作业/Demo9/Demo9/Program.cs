﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo9
{
    class Program
    {
        static void Main(string[] args)
        {
            //8.	实现查找数组元素索引的功能
            //定义一个数组，然后控制台输入要查找的元素，返回输入值在数组中最后一次出现的位置。若是找不到，请打印找不到。
            int[] a = { 5, 3, 2, 4, 6 };
            for (int i = 0; i < a.Length;i++)
            {
                Console.WriteLine("请输入要查找的元素");
                int n = int.Parse(Console.ReadLine());
                int index = Array.LastIndexOf(a,n);
                if (n == a[i])
                {
                    Console.WriteLine("该元素在数组中最后一次出现的位置是："+index);
                }
                else if (index == -1) {
                    Console.WriteLine("找不到");
                }
            }
        }
    }
}
