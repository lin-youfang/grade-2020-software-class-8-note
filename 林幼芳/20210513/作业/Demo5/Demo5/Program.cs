﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo5
{
    class Program
    {
        static void Main(string[] args)
        {
            //4.	接受用户输入一个字符
            //后判断这个字母是否为元音字母（不区分大小写），元音字母为A、E、I、O、U。用switch case实现；
            Console.WriteLine("请输入一个字符");
            char zf = char.Parse(Console.ReadLine());
            switch (zf)
            {
                case 'A':
                case 'E':
                case 'I':
                case 'O':
                case 'U':
                    Console.WriteLine("是元音字母");
                    break;
                default:
                    Console.WriteLine("不是元音字母");
                    break;
            
            
            
            }
            Console.ReadKey();
        }
    }
}
