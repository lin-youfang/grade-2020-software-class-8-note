﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.求圆的面积
            // 要求用户输入半径的值，打印出以此值为半径的圆的面积
            // 求圆的面积的公式：πr²。
            // 圆周率π定义成常量取3.14。
            // r由用户输入并存入一个变量中，此变量用double比较合适，因为用户可能会输入小数

            Console.WriteLine("请输入半径r");
            double r = double.Parse(Console.ReadLine());
            const double Π= 3.14;
            Console.WriteLine("圆的面积为："+Π*r*r);

        }
    }
}
