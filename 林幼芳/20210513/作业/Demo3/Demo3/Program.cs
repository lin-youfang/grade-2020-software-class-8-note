﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo3
{
    class Program
    {
        static void Main(string[] args)
        {
            //2.编写一个程序，请用户输入一个四位整数，将用户输入的四位数的千位、百位、十位和个位分别显示出来，
            //如5632，则显示“用户输入的千位为5，百位为6，十位为3，个位为2”
            Console.WriteLine("请输入一个四位整数");
            int num = int.Parse(Console.ReadLine());
            int a, b, c, d;
            Console.WriteLine("千位是：" + num / 1000);
            Console.WriteLine("百位是：" + num/100%10);
            Console.WriteLine("十位是：" + num / 10 % 10);
            Console.WriteLine("个位是：" + num %10);
            Console.ReadKey();
        }
    }
}
