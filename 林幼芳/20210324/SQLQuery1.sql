create database Bank;
use Bank;
create table Accountlnfo(
AccountId int identity(1,1) not null,
AccountCode nvarchar(20) not null,
AccountPhone nvarchar(20),
RealName nvarchar(20) not null,
);
create table BankCard(
CardNo nvarchar(30) primary key,
AccountId int not null,
CardPwd nvarchar(30) not null,
CardBalance money not null,
CardState tinyint not null,
CardTime nvarchar(30), 

);
create table CardExchange(
ExchangeId int identity(1,1) primary key,
CardNo nvarchar(30) not null,
MoneylnBank money not null,
ExchangeTime smalldatetime not null,
);
alter table  Accountlnfo add OpenTime smalldatetime not null;
alter table BankCard alter column CardTime smalldatetime not null;
alter table BankCard add constraint  DF_BankCard_CardTime default'getdate' for CardTime;
alter table Accountlnfo add constraint PK_AccountInfo_AccountId primary key(AccountId);
alter table Accountlnfo add constraint UQ_Accountlnfo_AccountCode unique(AccountCode);
alter table Accountlnfo add constraint DF_AccountInfo_OpenTime default'getdate()' for OpenTime ;
alter table Accountlnfo alter column AccountPhone nvarchar(20) not null;
alter table BankCard add constraint DF_BankCard_CardBalance default'0.00' for CardBalance;
alter table BankCard add constraint DF_BankCard_CardState default'1' for CardState;
alter table BankCard add constraint PK_BankCard_AccountId foreign key (AccountId)
   references Accountlnfo(AccountId);
alter table CardExchange add constraint PK_CardExchange_CardNo foreign key (CardNo)
   references BankCard(CardNo);
alter table CardExchange add constraint CK_CardExchange_MoneylnBank check(MoneyInBank >=0);
