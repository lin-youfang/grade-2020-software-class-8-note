

-----------------------------------
--内连接（inner join）：将两张表的所有记录进行一对一的连接，只返回符合连接条件的记录
-----------------------------------

--示例1，查询 所有学生信息 和 所在的班级信息（两表所有列）

select * from Student inner join Class on Class.ClassId=Student.StudentId

--示例2，查询 所有学生信息 和 所在的班级名称

select * from Student
select * from Class
select Student.*,ClassName from Student inner join Class on Class.ClassId=Student.ClassId

--示例3，查询 李逍遥(编号id为9) 所在的班级，显示学生id、姓名、班级名称（连接查询 2表）：

select StudentId,StudentName,ClassName from Class inner join Student on Class.ClassId=Student.StudentId where StudentId='3'

--示例4，查询 软件1班 学习的课程有哪几门，显示班级id、课程名称、课程学分（连接查询）：
select * from Class;
select * from ClassCourse;
select * from Score
select * from Class 
inner join ClassCourse on Class.ClassId=ClassCourse.ClassId 
inner join Score on Score.CourseId=ClassCourse.CourseId
where ClassName='软件一班'

--示例5：查询学生的成绩信息：学号，姓名，课程编号，成绩字段

select * from Score
select * from Student
select * from Course
select StudentName,Score from Score 
inner join Student on Score.StudentId=Student.StudentId
inner join Course on Course.CourseId=Score.CourseId

--示例6：查询学生的成绩信息：学号，姓名，课程名称，成绩字段
select StudentName,CourseName,Score from Score 
inner join Student on Score.StudentId=Student.StudentId
inner join Course on Score.CourseId=Course.CourseId



--示例7，查询 李逍遥 学习的课程有哪几门，显示学生id、姓名、课程名称、课程学分（连接查询）：
select *  from Student
select * from Course
 inner join ClassCourse on Course.CourseId=ClassCourse.ClassCourseId
 inner join Student on Student.ClassId=ClassCourse.ClassId
 where StudentName='陈美'





