---------------------------------------------------------------
--子查询:将查询语句作为另一个查询语句的一部分，就称为子查询
---------------------------------------------------------------
--示例1，查询钟琪和曾小林的考试成绩
--(1)查询出钟琪和曾小林的学号信息 为5和6
select StudentId from Student where StudentName='钟琪' or StudentName='曾小林'

--（2）将查询出来的学号作为成绩表的筛选条件值
select * from Score where StudentId in (select StudentId from Student where StudentName='钟琪' or StudentName='曾小林')



--示例2，查询不为软件1班和软件2班的学生信息
select * from Student where ClassId not in(select ClassId from Class where ClassName='软件一班' or ClassName='软件二班')

select * from Student where ClassId not in(select ClassId from Class where ClassName  in('软件一班' , '软件二班'))


--示例3：显示成绩第一名的成绩信息
select top 1 * from Score order by Score desc

select * from Score where Score=(select max(Score) from Score) 

--示例4：显示成绩第一名的学生信息
select * from Student where StudentId in
(select StudentId from Score where Score=(select max(Score) from Score) )



