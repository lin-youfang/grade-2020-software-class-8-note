﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo01
{
    class Program
    {
        static void Main(string[] args)
        {
            //统计下面一段文字中“类”字和“码”的个数。
            string str1 = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
            Count2(str1);
            Count3(str1);
            //1.循环遍历数组
            int sum = 0;
            int sum1 = 0;
            foreach (char item in str1)
            {
                if (item == '类')
                {
                    sum = sum + 1;
                }
                else if (item == '码')
                {
                    sum1 = sum1 + 1;
                }

            }
            Console.WriteLine("类字的个数为：" + sum);
            Console.WriteLine("码字的个数为：" + sum1);


        }


        public static void Count2(string str1)
        {
            //2、使用Replace方法来实现。
            string sum = str1.Replace("类","");
            string sum1 = str1.Replace("码","");
            Console.WriteLine("类的个数为："+(str1.Length-sum.Length));
            Console.WriteLine("码的个数为：" + (str1.Length - sum1.Length));


        }
        public static void Count3(string str1) {
            //3、使用Split()方法来实现。
            char[] lei = new char[] {'类'};
           string [] sum= str1.Split(lei);

            char[] ma = new char[] { '码'};
            string[] sum1 = str1.Split(ma);

            Console.WriteLine("类字的个数为："+(sum.Length-1));
            Console.WriteLine("码字的个数为："+(sum1.Length-1));


        }

    }
}
