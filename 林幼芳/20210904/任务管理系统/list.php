<?php

$dsn = "sqlsrv:Server=localhost;Database=Task";
$db = new PDO($dsn, "sa", "123456");

$sql = 'select * from Task order by TaskId desc;';
$result = $db->query($sql);
$taskList = $result->fetchAll(PDO::FETCH_ASSOC);
//var_dump($taskList);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>列表</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
</head>
<body>
<div id="container">
    <div id="login_info">
        欢迎你：admin
        <a href="logout.html">退出登录</a>
    </div>
    <div id="choose_div">
        <button id="all">全选</button>
        <a id="multi_delete" href="javascript:void(0);">删除选中任务</a>
        <a id="add" href="add.php" style="float: right">增加任务</a>
    </div>
    <table class="list">
        <tbody>
        <tr>
            <th></th>
            <th>任务id</th>
            <th>任务名称</th>
            <th>任务状态</th>
            <th>创建时间</th>
            <th>修改时间</th>
            <th>操作</th>
        </tr>
        <?php foreach($taskList as $key => $value):?>
            <tr>
                <td><input type="checkbox" class="task-checkbox" name="TaskId[]" value=""></td>
                <td><?php echo $value['TaskId'];?></td>
                <td><?php echo $value['TaskName'];?></td>
                <td><?php  if($value['TaskStatus']==1){
                    echo "新创建";
                    }else if($value['TaskStatus']==2){
                    echo "进行中";
                    }else if($value['TaskStatus']==3){
                    echo "已完成";
                    }
                ?></td>
                <td><?php echo $value['TaskCreateTime'];?></td>
                <td><?php echo $value['TaskUpdateTime'];?></td>
                <td><a class="update_ing" href="detail.php?task_id=<?php echo $value['TaskId']; ?>">详情</a>
                <a class="delete" href="delete.php?task_id=<?php echo $value['TaskId']; ?>">删除</a>
                </td>
            </tr>
        <?php endforeach;?>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>

