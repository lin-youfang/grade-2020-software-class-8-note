<?php
//2. 声明任意5个整数、浮点数、字符串、布尔值、数组类型的变量。
//3. 已经一个变量值为：100，检测是否是数字
//4. 已经一个变量值为："1"，检测是否是数字
//5. 已经一个变量值为："01"，检测是否是数字
//6. 已经一个变量值为："10f"，检测是否是数字
//7. 已经一个变量值为："0x10a"，检测是否是数字
//8. 已经一个变量值为："Hello"，检测是否是数组
//9. 已经一个变量值为：[]，检测是否是数组

$var=100;
 var_dump($var);
var_dump(is_numeric($var));
echo "<br />";


$var="1";
 var_dump($var);
var_dump(is_numeric($var));
echo "<br />";


$var="01";
 var_dump($var);
var_dump(is_numeric($var));
echo "<br />";

$var="10f";
 var_dump($var);
var_dump(is_numeric($var));
echo "<br />";

$var="0x10a";
 var_dump($var);
var_dump(is_numeric($var));
echo "<br />";


$var="Hello";
 var_dump($var);
var_dump(is_array($var));
echo "<br />";

$name="陆文霞";
var_dump($name);
echo "<br />";

$array=[1,2,3,4];
var_dump($array);
echo "<br />";

$f = 10.01;
var_dump((int)$f);
echo "<br />";
$d=2.15;
var_dump((int)$d);