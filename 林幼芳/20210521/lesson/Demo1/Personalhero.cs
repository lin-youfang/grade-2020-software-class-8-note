﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Personalhero:Hero
    {
        private string skill;
        public string Skill {
            get {
                return skill;
            }
            set {
                skill = value;
            }
        }
        public Personalhero() { 
        
        }
        public Personalhero(string skill) {
            this.skill = skill;
        }
        public new void Print() {
            base.Print();
            Console.WriteLine("角色能力为："+this.Skill);
        
        }

    }
}
