﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Hero
    {
        //3.使用类来描述游戏中的角色。
        //在很多RPG游戏中，第一次打开游戏，都会先让你创建自己的“英雄”，或者自己要扮演的角色。这些英雄或者角色都是我们游戏中的“对象”，
        //所以在开发的时候，我们需要针对每个角色都要写相应的类来描述。
        //见英雄文件夹的图片。
        //分析1: 角色具有以下信息（简单数据）
        //字段：角色名字，角色介绍，角色昵称，攻击力，防御力，速度。
        //方法：每个角色都有三个不同的攻击技能。
        //分析2：四个英雄的公共数据？公共数据向上抽取，抽象成一个Hero类，然后四个英雄继承这个Hero类，然后编写各自特有的类成员。

        protected string heroName;
        protected string introduce;
        protected string nicName;
        protected string atk;
        protected string defence;
        protected string speed;

        public string HeroName { get => heroName; set => heroName = value; }
        public string Introduce { get => introduce; set => introduce = value; }
        public string NicName { get => nicName; set => nicName = value; }
        public string Atk { get => atk; set => atk = value; }
        public string Defence { get => defence; set => defence = value; }
        public string Speed { get => speed; set => speed = value; }

        public Hero() { 
        
        }
        public Hero(string heroName,string introduce,string nicName,string atk,string defence,string speed) {
            this.heroName = heroName;
            this.introduce = introduce;
            this.nicName = nicName;
            this.atk = atk;
            this.defence = defence;
            this.speed = speed;       
        
        }
        public void Print() {
            Console.WriteLine("英雄名:"+HeroName);
            Console.WriteLine("介绍:"+Introduce);
            Console.WriteLine("昵称:"+NicName);
            Console.WriteLine("攻击力:"+Atk);
            Console.WriteLine("防御力:"+Defence);
            Console.WriteLine("速度:"+Speed);
        }
    }
}
