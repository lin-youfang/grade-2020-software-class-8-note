﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            //3.使用类来描述游戏中的角色。
            //在很多RPG游戏中，第一次打开游戏，都会先让你创建自己的“英雄”，或者自己要扮演的角色。这些英雄或者角色都是我们游戏中的“对象”，
            //所以在开发的时候，我们需要针对每个角色都要写相应的类来描述。
            //见英雄文件夹的图片。
            //分析1: 角色具有以下信息（简单数据）
            //字段：角色名字，角色介绍，角色昵称，攻击力，防御力，速度。
            //方法：每个角色都有三个不同的攻击技能。
            //分析2：四个英雄的公共数据？公共数据向上抽取，抽象成一个Hero类，然后四个英雄继承这个Hero类，然后编写各自特有的类成员。
            Hero hero = new Hero();
            hero.HeroName = "埃洛克";
            hero.Introduce = "埃洛克是一个来自末日边境的勇士。他是圣约英雄中名副其实的拳术好手。他用毁灭性的符文魔法和无情的拳术攻击消灭敌人";
            hero.Atk = "80%";
            hero.Defence = "60%";
            hero.Speed = "35%";
            Personalhero personalhero = new Personalhero();
            personalhero.Skill = "碎石打击、烈焰锚钩、战斗咆哮";
            hero.Print();

            Hero hero1 = new Hero();
            hero1.HeroName = "泰拉";
            hero1.Introduce = "泰拉是为复仇而来的勇者。她挥舞法杖，将愤怒转化为强大的元素魔法和攻击力，因此战无不胜";
            hero1.Atk = "80%";
            hero1.Defence = "60%";
            hero1.Speed = "50%";
            Personalhero personalhero1 = new Personalhero();
            personalhero1.Skill = "巨浪冲击、元素突击、复仇杀戮";
            hero.Print();

            Hero hero2 = new Hero();
            hero2.HeroName = "卢卡斯";
            hero2.Introduce = "卢卡斯是一名彬彬有礼的剑客，能控制源质能量。他一手持剑战斗，另一手辅助攻击";
            hero2.Atk = "80%";
            hero2.Defence = "30%";
            hero2.Speed = "50%";
            Personalhero personalhero2 = new Personalhero();
            personalhero2.Skill = "减速陷阱、能量浪潮、旋风剑舞";
            hero.Print();

            Hero hero3 = new Hero();
            hero3.HeroName = "洛菲";
            hero3.Introduce = "洛菲是一位攻击迅猛且擅长传送魔法的时空旅行者，喜欢利用他的幻想伙伴迷惑、吸引并摧毁敌人";
            hero3.Atk = "75%";
            hero3.Defence = "28%";
            hero3.Speed = "80%";
            Personalhero personalhero3 = new Personalhero();
            personalhero3.Skill = "能量精灵、暗影传送、时空迸裂";
            hero.Print();




        }
    }
}
