﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.假设要完成一个学校的校园管理信息系统，在员工管理系统中有不同的人员信息，包括学生信息、教师信息等。
            //学生的字段：编号（Id）、姓名（Name）、性别（Sex）、身份证号（Cardid）、联系方式（Tel）、专业（Major）、年级（Grade）
            //教师的字段：编号（Id）、姓名（Name）,性别（Sex）、身份证号（Cardid）、联系方式（Tel）、职称（Title）、工资号（Wageno）

            Campus campus = new Campus("001","黄月婷","女","350583200111156644","17359753616");
            Student student = new Student("电子商务","2020级电商1班");
            campus.Print();
            student.Print();
            Campus campus1 = new Campus("002","陈杰斌","男","35058319780010","1478536923");
            Teacher teacher = new Teacher("高级教师","28356479");
            campus1.Print();
            teacher.Print();
        }
    }
}
