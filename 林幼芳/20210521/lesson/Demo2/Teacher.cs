﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Teacher:Campus
    {

        //职称（Title）、工资号（Wageno）
        private string title;
        private string wageno;

        public string Title { get => title; set => title = value; }
        public string Wageno { get => wageno; set => wageno = value; }

        public new void Print() {
            
            Console.WriteLine("职称："+Title);
            Console.WriteLine("工资号："+Wageno);
        }
        public Teacher() { 
        
        }
        public Teacher(string title,string wageno) {
            this.title = title;
            this.wageno = wageno;
        }
    }
}
