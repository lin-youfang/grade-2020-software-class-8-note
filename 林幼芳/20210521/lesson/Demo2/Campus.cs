﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Campus
    {
        //1.假设要完成一个学校的校园管理信息系统，在员工管理系统中有不同的人员信息，包括学生信息、教师信息等。
        //学生的字段：编号（Id）、姓名（Name）、性别（Sex）、身份证号（Cardid）、联系方式（Tel）、专业（Major）、年级（Grade）
        //教师的字段：编号（Id）、姓名（Name）,性别（Sex）、身份证号（Cardid）、联系方式（Tel）、职称（Title）、工资号（Wageno）
        protected string id;
        private string name;
        private string sex;
        private string cardid;
        private string tel;

        public string Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Sex { get => sex; set => sex = value; }
        public string Cardid { get => cardid; set => cardid = value; }
        public string Tel { get => tel; set => tel = value; }


        public Campus(){

            }
        public Campus(string id,string name,string sex,string cardid,string tel) {
            this.id = id;
            this.name = name;
            this.sex = sex;
            this.cardid = cardid;
            this.tel = tel;
        }

        public void Print() {
            Console.WriteLine("编号："+Id);
            Console.WriteLine("姓名："+Name);
            Console.WriteLine("性别"+Sex);
            Console.WriteLine("身份证号："+Cardid);
            Console.WriteLine("联系方式："+Tel);
        }
    }
}
