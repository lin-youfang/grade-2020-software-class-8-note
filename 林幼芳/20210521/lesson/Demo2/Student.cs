﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Student:Campus

    {
        private string major;
        private string grade;
        
        public string Major { get => major; set => major = value; }
        public string Grade { get => grade; set => grade = value; }

        public Student() { 
        
        }
        public Student(string major,string grade) {
            this.major = major;
            this.grade = grade;
        }
        public new void Print() {
            
            Console.WriteLine("专业："+Major);
            Console.WriteLine("年级："+Grade);
        }
    }
}
