import java.util.Arrays;
import java.util.Scanner;

public class login {
	static	String [][] user=new String [100][4];
	static	String [][] book=new String [100][5];
	static  String [][] pubCom=new String [100][3];
	static Scanner sc=new Scanner(System.in);
	public static void main(String[] args) {
		
				//初始化数据
				//第1个用户信息：行下标 0
				user[0][0] = " HP001"; //部门
				user[0][1] = "ken";//用户名
				user[0][2] ="123";//密码
				user[0][3] ="admin";//角色
				
				//第2个用户信息：行下标 1
				user[1][0] = " HP001"; //部门
				user[1][1] = "ken";//用户名
				user[1][2] ="123";//密码
				user[1][3] ="admin";//角色
				
				
				user[2][0] = "HP003";
				user[2][1] = "guile";
				user[2][2] = "123";
				user[2][3] = "user";

				//书籍信息初始化
				//第1个书籍信息：行下标 0
				book[0][0] = "001";			//isbn编码
				book[0][1] = "JAVA高级编程";	//书籍名称
				book[0][2] = "100";			//价格
				book[0][3] = "铁道部出版社";	//出版社
				book[0][4] = "张三";			//作者

				//第2个书籍信息：行下标 1
				book[1][0] = "002";
				book[1][1] = "SQLSERVER高级编程";
				book[1][2] = "150";
				book[1][3] = "清华出版社";
				book[1][4] = "李四";

				//第3个书籍信息：行下标 2
				book[2][0] = "003";
				book[2][1] = "JAVA基础入门";
				book[2][2] = "50";
				book[2][3] = "铁道部出版社";
				book[2][4] = "张三";

				book[3][0] = "004";
				book[3][1] = "ORACLE高级编程";
				book[3][2] = "120";
				book[3][3] = "清华出版社";
				book[3][4] = "王五";

				//出版社信息初始化
				//第1个出版社信息：行下标 0
				pubCom[0][0] = "清华出版社";
				pubCom[0][1] = "武汉市XXXX";
				pubCom[0][2] = "aaa";

				//第2个出版社信息：行下标 1
				pubCom[1][0] = "铁道出版社";
				pubCom[1][1] = "北京市XXXX";
				pubCom[1][2] = "bbb";

				pubCom[2][0] = "邮电出版社";
				pubCom[2][1] = "南京市XXXX";
				pubCom[2][2] = "ccc";
		
		boolean flag=true;
		while(flag) {
			System.out.println("***********欢迎使用图书管理系统*************");
			System.out.println("请输入用户名");
			String username=sc.next();
			System.out.println("请输入密码");
			String password=sc.next();
			boolean find=false;
			for (int i = 0; i < user.length; i++) {
				if(username.equals(user[i][1]) && password.equals(user[i][2])) {
					find=true;
					break;
					
				}
			}
			if(find) {
				System.out.println("登陆成功");
				while(true) {
					System.out.println("请输入1.图书管理2.出版社管理3.退出系统");
					int c=sc.nextInt();
					if(c==1) {
						//图书管理
						BookMenu();
					}
					else if(c==2) {
						//出版社管理
						BookPub();
						
					}
					else if(c==3) {
						//退出系统
						System.exit(0);
						
					}
					else {
						System.out.println("请输入正确的数字");
					}
						
				}
				
			}
			else {
				System.out.println("用户不存在或账号密码错误");
			}
		}
		
				
	}

private static void BookPub() {
	while(true) {
		System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
		System.out.println("请输入数字选择功能菜单：");
		int c=  sc.nextInt();
		if(c == 1) {
			//1.图书增加
			pubAdd();
			
		}
		else if(c == 2) {
			//2.图书删除
			pubdelete();
		}
		else if(c == 3) {
			//3.更新
			pubupdate();
			
		}
		else if(c == 4) {
			//4.查询
			pubselect();

		}
		else if(c == 5) {
			//5.返回上一级菜单
			break;
		}
		else{
			//选择错误
			System.out.println("输入错误请重新选择");
		}
	}
		
		
	}

private static void pubselect() {
	System.out.println("出版社 \t");
	for (int i = 0; i < pubCom.length; i++) {
		if(pubCom[i] != null && pubCom[i][0] != null) {
			for (int j = 0; j < pubCom[j].length; j++) {
				
			System.out.print(pubCom[i][0] +"\t");
				
			}
			System.out.println();
		}
	}
}

private static void pubupdate() {
	
	bookupdate();
}

private static void pubdelete() {
	
	
}

private static void pubAdd() {
	bookAdd();
	
}

private static void BookMenu() {
		
		while(true) {
			System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
			System.out.println("请输入数字选择功能菜单：");
			int c=  sc.nextInt();
			if(c == 1) {
				//1.图书增加
				bookAdd();
				
			}
			else if(c == 2) {
				//2.图书删除
				bookdelete();
			}
			else if(c == 3) {
				//3.更新
				bookupdate();
				
			}
			else if(c == 4) {
				//4.查询
				bookselect();

			}
			else if(c == 5) {
				//5.返回上一级菜单
				break;
			}
			else{
				//选择错误
				System.out.println("输入错误请重新选择");
			}
		}
		
		
		
	}

	private static void bookselect() {
		System.out.println("a)	根据ISBN查询\r\n" + 
				           "b)	根据书名查询（模糊）\r\n" + 
				           "c)	根据出版社查询\r\n" + 
				           "d)	根据作者查询\r\n" + 
				           "e)	根据价格范围查询\r\n" + 
				           "f)	查询所有书籍信息\r\n" + 
				           "g)	返回上一级菜单\r\n" + "");
		System.out.println("请输入要选择的功能菜单");
		int c=sc.nextInt();
		if(c==1) {
			System.out.println("请输入要查询书籍的isbn编码：");
			String isbn=sc.next();
			selectbyisbn(isbn);
			
		}
		else if(c==2) {
			System.out.println("请输入要查询的书籍名称");
			String bookName=sc.next();
			selectbyname(bookName);
			
		}
		else if(c==3) {
			System.out.println("请输入要查询的书籍出版社");
			String pub=sc.next();
			selectbypub(pub);
		}
		else if(c==4) {
			System.out.println("请输入书籍作者");
			String author=sc.next();
			selectbyauthor(author);
		}
		else if(c==5) {
			bookbyprice();
			
			
		}
		else if(c==6) {
			
			PrintAllBook();
			
		}
		else if(c==7) {
			
			
		}
		else {
			System.out.println("输入错误请重新输入");
			
		}
		

	
}

	

	private static void bookbyprice() {
		System.out.println("请输入最低价格");
		double min=sc.nextDouble();
		System.out.println("请输入最高价格");
		double max=sc.nextDouble();
		for (int i = 0; i < book.length; i++) {
			if(book[i][2]!=null) {
			double bookprice=Double.parseDouble(book[i][2]);
			if(bookprice<=max && bookprice>=min) {
				for (int j = 0; j < book[i].length; j++) {
					System.out.print(book[i][j]+"\t");
				}
				
				System.out.println();
			}
			}
			}
		}
		
	

	private static void selectbyauthor(String author) {
		System.out.println("书籍isbn \t 书籍名称 \t 书籍价格 \t 出版社 \t 作者");
		int index = getIndexByauthor(author);
		for (int i = 0; i < book[index].length; i++) {
			System.out.print(book[index][i]+"\t");
		}
			System.out.println();
		
		
	}

	private static int getIndexByauthor(String author) {
		int index = -1;
		for (int i = 0; i < book.length; i++) {
			if(author.equals(book[i][4])) {
				index = i;
				break;
			}
		}
		
		
		return index;
		
	}

	private static void selectbypub(String pub) {
		int index = getIndexBypub(pub);
		System.out.println("书籍isbn \t 书籍名称 \t 书籍价格 \t 出版社 \t 作者");
		for (int i = 0; i < book[index].length; i++) {
			System.out.print(book[index][i]+"\t");
		}
			System.out.println();
		
		
	}

	private static int getIndexBypub(String pub) {
		int index = -1;
		for (int i = 0; i < book.length; i++) {
			if(pub.equals(book[i][3])) {
				index = i;
				break;
			}
		}
		
		
		return index;
		
	}

	private static void selectbyname(String bookName) {
		for (int i = 0; i < book.length; i++) {
			if(book[i][1]!=null && book[i][1].indexOf(bookName) !=-1) {
				for (int j = 0; j < book[i].length; j++) {
					System.out.print(book[i][j]+"\t");
				}
				System.out.println();
			}
		}
		
	}

	private static void selectbyisbn(String isbn) {
		int index = getIndexByisbn(isbn);
		System.out.println("书籍isbn \t 书籍名称 \t 书籍价格 \t 出版社 \t 作者");
		for (int i = 0; i < book[index].length; i++) {
			System.out.print(book[index][i]+"\t");
		}
			System.out.println();
		
		
			}
		
		
	

	private static void bookupdate() {
	System.out.println("请输入要更新的书籍编号：");
	String isbn=sc.next();
	System.out.println("请输入要更新的书籍名称");
	String bookName=sc.next();
	System.out.println("请输入要更新的书籍价格");
	String price=sc.next();
	System.out.println("请输入要更新的书籍出版社");
	String pub=sc.next();
	System.out.println("请输入要更新的书籍作者");
	String author=sc.next();
	
	
	int index = getIndexByisbn(isbn);
	if(index !=-1) {
		
		move(isbn,bookName,price,pub,author,index);
	}
	else {
		System.out.println("没有找到要更新的书籍信息");
	}
	
}


	private static void move(String isbn, String bookName, String price, String pub, String author, int index) {
		book[index][0] = isbn;
		book[index][1] = bookName; 
		book[index][2] = price;
		book[index][3] = pub;
		book[index][4] = author;
		
	}

	private static void bookdelete() {
	System.out.println("请输入要删除的isbn编码：");
	String isbn=sc.next();
	int index = getIndexByisbn(isbn);
	if(index ==-1) {
		System.out.println("没有找到要删除的书籍信息");
		
	}else {
		
		book[index][0]=null;
		book[index][1]=null;
		book[index][2]=null;
		book[index][3]=null;
		book[index][4]=null;
		PrintAllBook();
		
	}
	
}

	private static void bookAdd() {
		System.out.println("请输入图书的编号");
		String isbn = sc.next();
		System.out.println("请输入图书的名称");
		String bookName = sc.next();
		System.out.println("请输入图书的价格");
		String price = sc.next();
		System.out.println("请输入图书的出版社");
		String pub = sc.next();
		System.out.println("请输入图书的作者");
		String author = sc.next();
		
	
		int index = getIndexByisbn(isbn);
		
		if(index == -1) {
			int i = getFirstNull();
			
			if(i != -1) {
				book[i][0] = isbn;
				book[i][1] = bookName; 
				book[i][2] = price;
				book[i][3] = pub;
				book[i][4] = author;
				System.out.println("增加成功");
				PrintAllBook();
			}
			else {
				System.out.println("书籍已满，无法添加");
			}
			
		}else {
			System.out.println("书籍信息已存在，添加失败");
		}
		
		
		
		
	}


	private static void PrintAllBook() {
		
	
		System.out.println("书籍isbn \t 书籍名称 \t 书籍价格 \t 出版社 \t 作者");
		for (int i = 0; i < book.length; i++) {
			if(book[i] != null && book[i][0] != null) {
				for (int j = 0; j < book[j].length; j++) {
					
				System.out.print(book[i][j] +"\t");
					
				}
				System.out.println();
			}
		}
	}

	private static int getFirstNull() {
		int index = -1;
		
		for (int i = 0; i < book.length; i++) {
			if(book[i][0] == null) {
				index = i;
				break;
			}
		}
		
		return index;
	}

	private static int getIndexByisbn(String isbn) {
		int index = -1;
		for (int i = 0; i < book.length; i++) {
			if(isbn.equals(book[i][0])) {
				index = i;
				break;
			}
		}
		
		
		return index;
	}


}
