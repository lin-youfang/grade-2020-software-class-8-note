package prj;

import java.util.Scanner;

public class Demo02 {

	public static void main(String[] args) {
	//1.声明4个函数，分别用来计算两个数的和、差、积、商
	Scanner sc=new Scanner(System.in);
	System.out.println("请输入计算方式1.和2.差3.积4.商");
	int n=sc.nextInt();
	System.out.println("请输第一个数");
	int a=sc.nextInt();
	System.out.println("请输入第二个数");
	int b=sc.nextInt();
	if(n==1) {
		sum(a,b);
	}else if(n==2) {
		var(a,b);
	}else if(n==3) {
		mul(a,b);
	}else if(n==4) {
		mod(a,b);
	}else {
		System.out.println("请输入1-4");
	}
	}
	
	public static void sum(int a,int b) {
		int sum=a+b;
		System.out.println("和："+sum);
	}
	public static void var(int a,int b) {
		int var=a-b;
		System.out.println("差："+var);
	}
	public static void mul(int a,int b) {
		int mul=a*b;
		System.out.println("积："+mul);
	}
	public static void mod(int a,int b) {
		int mod=a/b;
		System.out.println("商："+mod);
	}
}
