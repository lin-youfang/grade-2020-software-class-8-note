package prj;

import java.util.Scanner;

public class Demo05 {

	public static void main(String[] args) {
	// 5.定义一函数，用于求2个数中的较大数，并将其返回，这2个数字在主函数中由用户输入
	Scanner sc=new Scanner(System.in);
	System.out.println("请输入第一个数");
	int a=sc.nextInt();
	System.out.println("请输入第二个数");
	int b=sc.nextInt();
	int max=max(a,b);
	System.out.println(max);
	}
	public static int max(int a,int b) {
		int max;
		if(a>b) {
			max=a;
		}else {
			max=b;
		}
		return max;
	}

}
